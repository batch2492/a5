class Customer {
	constructor(email){
		this.email = email;
		this.cart = new Cart() ;
		this.orders = [];

	}

	checkOut(){
		
		
		if (this.cart.contents.length > 0){
			this.orders.push({products: this.cart.contents, totalAmount: this.cart.totalAmount})
		}
		
		return this;
	}
}

class Cart {
	constructor(){
		this.contents = [];
		this.totalAmount = 0;
	}

	addToCart(name, quantity){

		this.contents.push({name, quantity})
		return this;
	}

	showCartContents(){

		console.log(this.contents)
	}

	updateProductQuantity(product, quantity){
		this.contents.forEach(content => {
			if(content.name.name == product){
				content.quantity = quantity;
			}
		})
		return this;
	}

	clearCartContents(){
		this.contents = [];
		return this
	}

	computeTotal(){
		let total = 0;
		this.contents.forEach(content => {
			
			total += (content.name.price*content.quantity)
		})

		this.totalAmount = total;

		return this;
	}
}


class Product {
	constructor(name, price){
		this.name = name;
		this.price = price;
		this.isActive = true;
	}

	archive(){
		if (this.isActive){
			this.isActive = false;
		}
		return this;
	}

	updatePrice(newPrice){
		this.price = newPrice;
		return this;
	}
}

const John = new Customer("john@email.com");



const prodA = new Product('soap', 9.99);

const prodB = new Product('alcohol', 12.99);



John.cart.addToCart(prodA, 6);
//John.cart.addToCart(prodB, 8);

John.cart.computeTotal();

console.log(John);
